package ru.bluelime.opendata.adapter.exception;

public class ValidationException extends BasicException {
    public ValidationException(String code, String message) {
        super(code, message);
    }
}
