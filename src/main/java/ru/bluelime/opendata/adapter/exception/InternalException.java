package ru.bluelime.opendata.adapter.exception;

public class InternalException extends BasicException {
    public InternalException(String code, String message, Throwable cause) {
        super(code, message, cause);
    }

    public InternalException(String code, String message) {
        super(code, message);
    }
}
