package ru.bluelime.opendata.adapter.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public abstract class BasicException extends RuntimeException {
    private final String code;

    BasicException(String code, String message) {
        super(message);
        this.code = code;
    }

    BasicException(String code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }
}
