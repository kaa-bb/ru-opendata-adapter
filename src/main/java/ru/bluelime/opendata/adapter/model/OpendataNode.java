package ru.bluelime.opendata.adapter.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public abstract class OpendataNode {
    private String id;
}
