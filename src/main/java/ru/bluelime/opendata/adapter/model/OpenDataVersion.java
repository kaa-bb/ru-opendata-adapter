package ru.bluelime.opendata.adapter.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
public class OpenDataVersion {
	@Id
	@GeneratedValue
	private Long id;
	@NonNull
	private String created;
	@NonNull
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "order_id")
	private OpenDataDatasetInformation information;

	@Embedded
	private OpenDataContent content;
}