package ru.bluelime.opendata.adapter.model;

import lombok.Data;

import javax.persistence.*;

@Embeddable
@Data
public class OpenDataContent {
    @Column(length = 1048576)
    private String content;
}
