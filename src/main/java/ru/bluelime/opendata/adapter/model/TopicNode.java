package ru.bluelime.opendata.adapter.model;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

@Value
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class TopicNode extends OpendataNode {
    public TopicNode(String id) {
        super(id);
    }
}
