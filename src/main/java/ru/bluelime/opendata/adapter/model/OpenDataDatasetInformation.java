package ru.bluelime.opendata.adapter.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class OpenDataDatasetInformation {
    @Id
    private String identifier;
    private String creator;
    private LocalDateTime created;
    private String subject;
    private String format;
    @Column(length = 1048576)
    private String description;
    private LocalDateTime modified;
    private String title;

    @OneToMany(
            targetEntity = OpenDataVersion.class,
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            mappedBy = "information")
    private Collection<OpenDataVersion> versions;
}
