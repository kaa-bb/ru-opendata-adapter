package ru.bluelime.opendata.adapter.model;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

@Value
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class OrgenizationNode extends OpendataNode {
    public OrgenizationNode(String id) {
        super(id);
    }
}
