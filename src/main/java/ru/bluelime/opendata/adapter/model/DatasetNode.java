package ru.bluelime.opendata.adapter.model;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

@Value
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DatasetNode extends OpendataNode {
    public DatasetNode(String id) {
        super(id);
    }
}
