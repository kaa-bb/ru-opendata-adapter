package ru.bluelime.opendata.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import ru.bluelime.opendata.adapter.config.OpenDataApiProperties;
import ru.bluelime.opendata.adapter.config.ProxyProperties;

@SpringBootApplication
@Import({OpenDataApiProperties.class})
public class Application {
	@Autowired
	private ProxyProperties proxyProperties;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
