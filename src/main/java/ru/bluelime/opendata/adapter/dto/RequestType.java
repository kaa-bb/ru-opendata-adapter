package ru.bluelime.opendata.adapter.dto;

public enum RequestType {
    DATASET_LIST,
    DATASET,
    VERSION_LIST,
    VERSION,
    CONTENT
}
