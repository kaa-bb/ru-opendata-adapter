package ru.bluelime.opendata.adapter.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.lang.Nullable;

@Data
@ApiModel(description = "Запрос")
@ToString
public abstract class Request {
    @Nullable
    @ApiModelProperty(notes = "Идентификатор запроса", example = "7f0eefc3-f4f3-4f17-8011-60fb04e75a00", position = 1)
    private String id;
}
