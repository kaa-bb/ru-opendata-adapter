
package ru.bluelime.opendata.adapter.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "YearMonth",
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "Oktober",
        "November",
        "December",
        "CountWorkDays",
        "CountHolidays",
        "CountHours40",
        "CountHours36",
        "CountHours24"
})
public class WorkCalendarItemDto {

    @JsonProperty("Год/Месяц")
    public String yearMonth;
    @JsonProperty("Январь")
    public String january;
    @JsonProperty("Февраль")
    public String february;
    @JsonProperty("Март")
    public String march;
    @JsonProperty("Апрель")
    public String april;
    @JsonProperty("Май")
    public String may;
    @JsonProperty("Июнь")
    public String june;
    @JsonProperty("Июль")
    public String july;
    @JsonProperty("Август")
    public String august;
    @JsonProperty("Сентябрь")
    public String september;
    @JsonProperty("Октябрь")
    public String oktober;
    @JsonProperty("Ноябрь")
    public String november;
    @JsonProperty("Декабрь")
    public String december;
    @JsonProperty("Всего рабочих дней")
    public String countWorkDays;
    @JsonProperty("Всего праздничных и выходных дней")
    public String countHolidays;
    @JsonProperty("Количество рабочих часов при 40-часовой рабочей неделе")
    public String countHours40;
    @JsonProperty("Количество рабочих часов при 36-часовой рабочей неделе")
    public String countHours36;
    @JsonProperty("Количество рабочих часов при 24-часовой рабочей неделе")
    public String countHours24;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}