package ru.bluelime.opendata.adapter.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class OpenDataInformationRequest extends Request {
}
