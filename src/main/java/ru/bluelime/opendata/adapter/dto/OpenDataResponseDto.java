package ru.bluelime.opendata.adapter.dto;

import lombok.Value;

@Value
public class OpenDataResponseDto<T> {
    private String url;
    private String absoluteUrl;
    private T data;
}
