package ru.bluelime.opendata.adapter.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
@Data
public class OpenDataVersionDto {

	@JsonProperty("created")
	private String created;

}