package ru.bluelime.opendata.adapter.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.lang.Nullable;


@ApiModel(description = "Ответ")
@Data
@ToString
public class Response {
    @Nullable
    @ApiModelProperty(notes = "Код ошибки", example = "errorCode", required = false, position = 2)
    private String errorCode;
    @Nullable
    @ApiModelProperty(notes = "Текст ошибки", example = "error", required = false, position = 3)
    private String error;
}
