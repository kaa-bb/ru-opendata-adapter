package ru.bluelime.opendata.adapter.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class OpenDataDatasetInformationDto {

	@JsonProperty("identifier")
	private String identifier;

	@JsonProperty("creator")
	private String creator;

	@JsonProperty("created")
	private String created;

	@JsonProperty("subject")
	private String subject;

	@JsonProperty("format")
	private String format;

	@JsonProperty("description")
	private String description;

	@JsonProperty("modified")
	private String modified;

	@JsonProperty("title")
	private String title;
}