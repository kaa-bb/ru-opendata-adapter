package ru.bluelime.opendata.adapter.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import ru.bluelime.opendata.adapter.dto.OpenDataInformationRequest;
import ru.bluelime.opendata.adapter.dto.Request;
import ru.bluelime.opendata.adapter.dto.Response;
import ru.bluelime.opendata.adapter.exception.BasicException;
import ru.bluelime.opendata.adapter.exception.InternalException;
import ru.bluelime.opendata.adapter.exception.ValidationException;
import ru.bluelime.opendata.adapter.service.RestControllerService;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/v1")
@Api(value = "/api", tags = "api currency exchange v1 controller")
@SwaggerDefinition(tags = {
        @Tag(name = "api currency exchange v1 controller", description = "Операции сервиса версии v1")
})
@Slf4j
@RequiredArgsConstructor
public class OpenDataController {

    private final RestControllerService restControllerService;

    @ApiOperation(value = "")
    @PostMapping(value = "/getDatasetInformation", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public Response getDatasetInformation(@RequestBody OpenDataInformationRequest request) {
        logRequest("getDatasetInformation", request.toString());
        return restControllerService.handleRequest(request);
    }

    @ApiOperation(value = "")
    @PostMapping(value = "/directRequest", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public Response directRequest(@RequestBody Request request) {
        logRequest("directRequest", request.toString());
        return restControllerService.handleRequest(request);
    }

    @ExceptionHandler(value = {ValidationException.class, InternalException.class})
    public ResponseEntity<Response> handleException(RuntimeException e, WebRequest request) {
        Response response = new Response();
        response.setError(e.getMessage());
        HttpStatus resStatus;
        log.error("Throw error by request.", e);
        if (e instanceof ValidationException) {
            resStatus = HttpStatus.BAD_REQUEST;
        } else {
            resStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        response.setError(e.getMessage());
        response.setErrorCode(((BasicException) e).getCode());

        return new ResponseEntity<>(response, resStatus);
    }

    private void logRequest(String method, String request) {
        log.info("Call {}. request: {}, user: {}", method, request, "");
    }
}
