package ru.bluelime.opendata.adapter.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@ConfigurationProperties(prefix="opendata.parser")
@Component
@Data
public class OpenDataParserProperties {
    private List<String> dataset;
    private List<String> organization;
    private List<String> topic;
}
