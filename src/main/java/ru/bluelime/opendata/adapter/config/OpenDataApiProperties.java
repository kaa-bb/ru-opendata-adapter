package ru.bluelime.opendata.adapter.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix="opendata.api")
@Data
public class OpenDataApiProperties {
    private String link;
    private String key;
    private String format;
}
