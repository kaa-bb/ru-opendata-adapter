package ru.bluelime.opendata.adapter.config;

import io.micrometer.core.instrument.util.StringUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Properties;


@ConfigurationProperties(prefix = "application.proxy")
@Component
@Data
public class ProxyProperties {
    private String host;
    private String port;
    private String noProxy;

    public boolean isProxySet() {
        return StringUtils.isNotBlank(host);
    }

    @PostConstruct
    void started() {
        Properties systemProps = System.getProperties();
        if (this.isProxySet()) {
            systemProps.put("proxySet", "true");
            systemProps.put("proxyHost", this.host);
            systemProps.put("proxyPort", this.port);
            systemProps.put("nonProxyHosts", this.noProxy);
            systemProps.put("http.nonProxyHosts", this.noProxy);
            systemProps.put("https.nonProxyHosts", this.noProxy);
        }
    }
}
