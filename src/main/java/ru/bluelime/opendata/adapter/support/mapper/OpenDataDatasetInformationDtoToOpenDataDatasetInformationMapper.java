package ru.bluelime.opendata.adapter.support.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.bluelime.opendata.adapter.dto.OpenDataDatasetInformationDto;
import ru.bluelime.opendata.adapter.model.OpenDataDatasetInformation;
import ru.bluelime.opendata.adapter.support.MappingConfiguration;

@Mapper(config = MappingConfiguration.class)
public interface OpenDataDatasetInformationDtoToOpenDataDatasetInformationMapper {
    @Mapping(target = "identifier", source = "source.identifier")
    @Mapping(target = "creator", source = "source.creator")
    @Mapping(target = "created", source = "source.created", dateFormat = "yyyyMMdd'T'HHmmss")
    @Mapping(target = "subject", source = "source.subject")
    @Mapping(target = "format", source = "source.format")
    @Mapping(target = "description", source = "source.description")
    @Mapping(target = "modified", source = "source.modified", dateFormat = "yyyyMMdd'T'HHmmss")
    @Mapping(target = "title", source = "source.title")
    OpenDataDatasetInformation convert(OpenDataDatasetInformationDto source);
}
