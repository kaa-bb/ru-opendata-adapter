package ru.bluelime.opendata.adapter.support.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.bluelime.opendata.adapter.dto.OpenDataInformationResponse;
import ru.bluelime.opendata.adapter.model.OpenDataDatasetInformation;
import ru.bluelime.opendata.adapter.support.MappingConfiguration;

@Mapper(config = MappingConfiguration.class)
public interface OpenDataDatasetInformatioToRsponseMapper {
    @Mapping(target = "id", source = "source.identifier")
    OpenDataInformationResponse convert(OpenDataDatasetInformation source);
}
