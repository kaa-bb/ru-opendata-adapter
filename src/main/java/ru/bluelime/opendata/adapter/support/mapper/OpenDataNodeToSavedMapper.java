package ru.bluelime.opendata.adapter.support.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.bluelime.opendata.adapter.model.OpenDataContent;
import ru.bluelime.opendata.adapter.support.MappingConfiguration;

@Mapper(config = MappingConfiguration.class)
public interface OpenDataNodeToSavedMapper {
    @Mapping(target = "content", source = "content")
    OpenDataContent convert(String content);
}
