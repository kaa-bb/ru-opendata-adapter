package ru.bluelime.opendata.adapter.support.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class OpenDataConstant {
    public static final String VERSION_URL = "version";
    public static final String DATASET_URL = "dataset";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String SLASH = "/";

}
