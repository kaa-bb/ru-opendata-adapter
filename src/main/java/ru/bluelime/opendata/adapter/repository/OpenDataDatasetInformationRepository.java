package ru.bluelime.opendata.adapter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.bluelime.opendata.adapter.model.OpenDataDatasetInformation;

public interface OpenDataDatasetInformationRepository extends JpaRepository<OpenDataDatasetInformation, String> {
}
