package ru.bluelime.opendata.adapter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.bluelime.opendata.adapter.model.OpenDataVersion;

public interface OpenDataVersionRepository extends JpaRepository<OpenDataVersion, Long> {
}
