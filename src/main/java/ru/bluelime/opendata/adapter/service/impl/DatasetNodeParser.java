package ru.bluelime.opendata.adapter.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.bluelime.opendata.adapter.dto.OpenDataDatasetInformationDto;
import ru.bluelime.opendata.adapter.dto.OpenDataResponseDto;
import ru.bluelime.opendata.adapter.dto.OpenDataVersionDto;
import ru.bluelime.opendata.adapter.model.DatasetNode;
import ru.bluelime.opendata.adapter.model.OpenDataDatasetInformation;
import ru.bluelime.opendata.adapter.model.OpenDataVersion;
import ru.bluelime.opendata.adapter.service.OpenDataAdapter;
import ru.bluelime.opendata.adapter.service.OpenDataService;

import java.util.Arrays;

@Component
@RequiredArgsConstructor
@Slf4j
public class DatasetNodeParser extends AbstractOpenDataParser<DatasetNode> {

    private final OpenDataAdapter openDataAdapter;
    private final OpenDataService openDataService;

    @Override
    protected void processNode(DatasetNode node) {
        OpenDataResponseDto responseDto = openDataAdapter.makeRequest(openDataAdapter.getDatasetUrl(node), OpenDataDatasetInformationDto.class);
        final OpenDataDatasetInformation information = openDataService.processDatasetInformation((OpenDataDatasetInformationDto) responseDto.getData());
        responseDto = openDataAdapter.makeRequest(openDataAdapter.getDatasetVersionUrl(node), OpenDataVersionDto[].class);
        processVersionArrayAvoidDublicate((OpenDataVersionDto[]) responseDto.getData(), node, information);
    }

    private void processVersionArrayAvoidDublicate(OpenDataVersionDto[] openDataVersionDtos, DatasetNode node, OpenDataDatasetInformation openDataDatasetInformation) {
        Arrays.stream(openDataVersionDtos)
                .distinct()
                .map(dto -> new OpenDataVersion(dto.getCreated(), openDataDatasetInformation))
                .forEach(e -> processContent(node, e));
    }

    private void processContent(DatasetNode node, OpenDataVersion version) {
        OpenDataResponseDto responseDto = openDataAdapter.makeRequest(openDataAdapter.getDatasetContentUrl(node, version.getCreated()), String.class);
        openDataService.processDatasetContent(version, responseDto.getData().toString());
    }

    @Override
    public String getType() {
        return DatasetNode.class.getName();
    }

}
