package ru.bluelime.opendata.adapter.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.bluelime.opendata.adapter.dto.Request;
import ru.bluelime.opendata.adapter.dto.Response;
import ru.bluelime.opendata.adapter.service.OpenDataService;
import ru.bluelime.opendata.adapter.service.RestControllerService;

@Service
@RequiredArgsConstructor
public class RestControllerServiceImpl implements RestControllerService {
    private final OpenDataService openDataService;

    @Override
    public Response handleRequest(Request request) {
        return openDataService.getNode(request.getId());
    }
}
