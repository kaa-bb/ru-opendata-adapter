package ru.bluelime.opendata.adapter.service;

import org.springframework.lang.NonNull;
import ru.bluelime.opendata.adapter.model.OpendataNode;

public interface OpenDataParserHandler {
    void processNode(@NonNull OpendataNode node);
    String getType();
}
