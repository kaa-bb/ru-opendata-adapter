package ru.bluelime.opendata.adapter.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.bluelime.opendata.adapter.dto.OpenDataDatasetInformationDto;
import ru.bluelime.opendata.adapter.dto.Response;
import ru.bluelime.opendata.adapter.model.OpenDataDatasetInformation;
import ru.bluelime.opendata.adapter.model.OpenDataVersion;
import ru.bluelime.opendata.adapter.repository.OpenDataDatasetInformationRepository;
import ru.bluelime.opendata.adapter.repository.OpenDataVersionRepository;
import ru.bluelime.opendata.adapter.service.OpenDataService;
import ru.bluelime.opendata.adapter.support.mapper.OpenDataDatasetInformatioToRsponseMapper;
import ru.bluelime.opendata.adapter.support.mapper.OpenDataDatasetInformationDtoToOpenDataDatasetInformationMapper;
import ru.bluelime.opendata.adapter.support.mapper.OpenDataNodeToSavedMapper;

@Service
@RequiredArgsConstructor
public class OpenDataServiceImpl implements OpenDataService {

    private final OpenDataDatasetInformationDtoToOpenDataDatasetInformationMapper openDataDatasetInformationMapper;
    private final OpenDataDatasetInformationRepository openDataDatasetInformationRepository;
    private final OpenDataVersionRepository openDataVersionRepository;
    private final OpenDataNodeToSavedMapper openDataNodeToSavedMapper;
    private final OpenDataDatasetInformatioToRsponseMapper openDataDatasetInformatioToRsponseMapper;

    @Override
    public OpenDataDatasetInformation processDatasetInformation(OpenDataDatasetInformationDto dto) {
        final OpenDataDatasetInformation information = openDataDatasetInformationMapper.convert(dto);
        openDataDatasetInformationRepository.save(information);
        return information;
    }

    @Override
    public void processDatasetContent(OpenDataVersion version, String data) {
        version.setContent(openDataNodeToSavedMapper.convert(data));
        openDataVersionRepository.save(version);
    }

    @Override
    public Response getNode(String id) {
        OpenDataDatasetInformation openDataDatasetInformation = openDataDatasetInformationRepository.findById(id).orElseThrow(InternalError::new);
        return openDataDatasetInformatioToRsponseMapper.convert(openDataDatasetInformation);
    }
}
