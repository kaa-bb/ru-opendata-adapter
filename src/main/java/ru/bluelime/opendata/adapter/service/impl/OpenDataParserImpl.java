package ru.bluelime.opendata.adapter.service.impl;

import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bluelime.opendata.adapter.config.OpenDataParserProperties;
import ru.bluelime.opendata.adapter.model.DatasetNode;
import ru.bluelime.opendata.adapter.model.OpendataNode;
import ru.bluelime.opendata.adapter.service.OpenDataParser;
import ru.bluelime.opendata.adapter.service.OpenDataParserHandler;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class OpenDataParserImpl implements OpenDataParser {
    private final Map<String, OpenDataParserHandler> parserMap;
    private final OpenDataParserProperties openDataParserProperties;

    @Autowired
    public OpenDataParserImpl(Set<OpenDataParserHandler> parserHandlerMap, OpenDataParserProperties openDataParserProperties) {
        this.openDataParserProperties = openDataParserProperties;
        Map<String, OpenDataParserHandler> tempMap = parserHandlerMap.stream()
                .collect(Collectors.toMap(OpenDataParserHandler::getType, e -> e));
        this.parserMap = Collections.unmodifiableMap(tempMap);
    }

    @Override
    public void processNode(OpendataNode node) {
        OpenDataParserHandler handler = parserMap.get(node.getClass().getName());
        if (Objects.isNull(handler)) {
            throw new IllegalArgumentException("Not supported");
        }
        handler.processNode(node);
    }

    @Override
    public void crawl() {
        openDataParserProperties.getDataset().forEach(e -> processNode(new DatasetNode(e)));
    }
}
