package ru.bluelime.opendata.adapter.service;

import ru.bluelime.opendata.adapter.dto.OpenDataDatasetInformationDto;
import ru.bluelime.opendata.adapter.dto.Response;
import ru.bluelime.opendata.adapter.model.OpenDataDatasetInformation;
import ru.bluelime.opendata.adapter.model.OpenDataVersion;

public interface OpenDataService {
    OpenDataDatasetInformation processDatasetInformation(OpenDataDatasetInformationDto dto);
    void processDatasetContent(OpenDataVersion version, String data);
    Response getNode(String id);
}
