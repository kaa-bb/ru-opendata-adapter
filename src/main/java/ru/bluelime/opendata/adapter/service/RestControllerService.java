package ru.bluelime.opendata.adapter.service;

import ru.bluelime.opendata.adapter.dto.Request;
import ru.bluelime.opendata.adapter.dto.Response;

public interface RestControllerService {
    Response handleRequest(Request request);
}
