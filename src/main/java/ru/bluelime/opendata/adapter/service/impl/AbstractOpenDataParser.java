package ru.bluelime.opendata.adapter.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import ru.bluelime.opendata.adapter.model.OpendataNode;
import ru.bluelime.opendata.adapter.service.OpenDataParserHandler;

@Component
@Slf4j
public abstract class AbstractOpenDataParser<T> implements OpenDataParserHandler {
    @Override
    @SuppressWarnings(value = "unchecked")
    public void processNode(@NonNull OpendataNode node) {
        T specificNode = (T) node;
        log.info("Start parse node {}", node);
        processNode(specificNode);
        log.info("Finish parse node {}", node);
    }

    protected abstract void processNode(@NonNull T node);
}
