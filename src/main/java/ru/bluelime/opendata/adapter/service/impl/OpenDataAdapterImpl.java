package ru.bluelime.opendata.adapter.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.bluelime.opendata.adapter.config.OpenDataApiProperties;
import ru.bluelime.opendata.adapter.dto.OpenDataResponseDto;
import ru.bluelime.opendata.adapter.exception.InternalException;
import ru.bluelime.opendata.adapter.model.OpendataNode;
import ru.bluelime.opendata.adapter.service.OpenDataAdapter;
import ru.bluelime.opendata.adapter.support.constant.OpenDataConstant;

import java.util.Objects;

@Component
@Slf4j
public class OpenDataAdapterImpl implements OpenDataAdapter {
    private final RestTemplate restTemplate;
    private final OpenDataApiProperties openDataApiProperties;
    private final String openApiBaseUrl;

    @Autowired
    public OpenDataAdapterImpl(RestTemplate restTemplate, OpenDataApiProperties openDataApiProperties) {
        this.restTemplate = restTemplate;
        this.openDataApiProperties = openDataApiProperties;
        this.openApiBaseUrl = getBaseUrl();
    }

    @Override
    @SuppressWarnings(value = "unchecked")
    public <T> OpenDataResponseDto<T> makeRequest(String url, Class<T> aClass) {
        final String absoluteUrl = this.openApiBaseUrl + url;
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(absoluteUrl)
                .queryParam(OpenDataConstant.ACCESS_TOKEN, openDataApiProperties.getKey());
        ResponseEntity<?> responseEntity = null;
        try {
            responseEntity = restTemplate.getForEntity(builder.build().encode().toUri(), aClass);
        } catch (Exception e) {
            log.error("Error when request {} ", absoluteUrl);
            throw new InternalException("error_response", "Error request " + absoluteUrl
                    + responseEntity);
        }
        if (Objects.isNull(responseEntity) || responseEntity.getStatusCode() != HttpStatus.OK) {
            log.error("Error when request {} response {}", absoluteUrl, responseEntity);
            throw new InternalException("error_response", "Error request " + absoluteUrl
                    + responseEntity);
        }
        return new OpenDataResponseDto(url, absoluteUrl,
                responseEntity.getBody());
    }

    @Override
    public String getDatasetUrl(OpendataNode dataset) {
        return OpenDataConstant.DATASET_URL + "/" + dataset.getId();
    }

    @Override
    public String getDatasetVersionUrl(OpendataNode dataset) {
        return OpenDataConstant.DATASET_URL + "/" + dataset.getId() + "/"
                + OpenDataConstant.VERSION_URL;
    }

    @Override
    public String getDatasetContentUrl(OpendataNode dataset, String version) {
        return OpenDataConstant.DATASET_URL + "/" + dataset.getId() + "/"
                + OpenDataConstant.VERSION_URL + "/" + version + "/"
                + "content";
    }

    private String getBaseUrl() {
        return openDataApiProperties.getLink() +
                openDataApiProperties.getFormat()
                + "/";
    }
}
